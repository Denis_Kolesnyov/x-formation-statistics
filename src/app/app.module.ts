import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent }  from './app.component';
import { ContributorsComponent } from './components/contributors/contributors.component';
import { RepositoriesComponent } from './components/repositories/repositories.component';
import { HttpModule } from '@angular/http';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/in-memory-data.service';
import { ContributorService } from './services/contributor.service';
import { RepositoryService } from './services/repository.service';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, {passThruUnknownUrl: true}), // , {passThruUnknownUrl: true}
    RouterModule.forRoot([
      {
        path: 'contributors',
        component: ContributorsComponent,
      },
      {
        path: 'repositories',
        component: RepositoriesComponent,
      },
      {
        path: '',
        redirectTo: '/contributors',
        pathMatch: 'full'
      },
    ]),
  ],
  exports: [ RouterModule ],
  declarations: [
    AppComponent,
    ContributorsComponent,
    RepositoriesComponent,
  ],
  bootstrap: [ AppComponent ],
  providers: [
    ContributorService,
    RepositoryService
  ],
})
export class AppModule { }

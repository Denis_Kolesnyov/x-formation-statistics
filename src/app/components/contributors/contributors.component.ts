import { Component, OnInit } from '@angular/core';
import { Contributor } from '../../classes/contributor';
import { ContributorService } from '../../services/contributor.service';
import { SortingService } from '../../services/sorting.service';


@Component({
  selector: 'contributors-list',
  templateUrl: './contributors.component.html',
  styleUrls: ['./contributors.component.css'],
})

export class ContributorsComponent implements OnInit {
  contributors: Contributor[];
  sortingOptions: any = {
    nickname:      { sorted: false, type: 'string' },
    team:          { sorted: false, type: 'string' },
    contributions: { sorted: false, type: 'number' },
  };
  SORT_DESC = 'descending';
  SORT_ASC = 'ascending';
  sortingOrder = this.SORT_DESC;

  loadContributorList(): void {
    this.contributorService.getContributors()
      .then(contributors => this.contributors = this.sortingService.sortContributorsByNumber(contributors, 'contributions'))
      .then(() => this.setSortingOptions('contributions'));
  }

  ngOnInit(): void {
    this.loadContributorList();
  }

  sortContributors(target: string): void {
    if (this.sortingOptions[target].sorted) {
      this.toggleSortingOrder();
      this.contributors.reverse();
    } else {
      if (this.sortingOptions[target].type === 'number') {
        this.sortByNumber(target);
      } else if (this.sortingOptions[target].type === 'string') {
        this.sortByString(target);
      }
      this.setSortingOptions(target);
      this.sortingOrder = this.SORT_DESC;
    }
  }

  sortByString(key: string): void {
    this.sortingService.sortContributorsByString(this.contributors, key);
  }

  sortByNumber(key: string): void {
    this.sortingService.sortContributorsByNumber(this.contributors, key);
  }

  setSortingOptions(target: string) {
    for (let key in this.sortingOptions) {
      this.sortingOptions[key].sorted = (key === target) ? true : false;
    }
  }

  toggleSortingOrder(): void {
    this.sortingOrder = (this.sortingOrder === this.SORT_DESC) ? this.SORT_ASC : this.SORT_DESC;
  }

  setSortArrowClass(target: string): string {
    return (!this.sortingOptions[target].sorted) ? 'not-sorted' :
      (this.sortingOptions[target].sorted && this.sortingOrder === this.SORT_DESC) ? 'sorted-descended' : 'sorted-ascended';
  }

  constructor(
    private contributorService: ContributorService,
    private sortingService: SortingService,
  ) {}
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var contributor_service_1 = require("../../services/contributor.service");
var sorting_service_1 = require("../../services/sorting.service");
var ContributorsComponent = (function () {
    function ContributorsComponent(contributorService, sortingService) {
        this.contributorService = contributorService;
        this.sortingService = sortingService;
        this.sortingOptions = {
            nickname: { sorted: false, type: 'string' },
            team: { sorted: false, type: 'string' },
            contributions: { sorted: false, type: 'number' },
        };
        this.SORT_DESC = 'descending';
        this.SORT_ASC = 'ascending';
        this.sortingOrder = this.SORT_DESC;
    }
    ContributorsComponent.prototype.loadContributorList = function () {
        var _this = this;
        this.contributorService.getContributors()
            .then(function (contributors) { return _this.contributors = _this.sortingService.sortContributorsByNumber(contributors, 'contributions'); })
            .then(function () { return _this.setSortingOptions('contributions'); });
    };
    ContributorsComponent.prototype.ngOnInit = function () {
        this.loadContributorList();
    };
    ContributorsComponent.prototype.sortContributors = function (target) {
        if (this.sortingOptions[target].sorted) {
            this.toggleSortingOrder();
            this.contributors.reverse();
        }
        else {
            if (this.sortingOptions[target].type === 'number') {
                this.sortByNumber(target);
            }
            else if (this.sortingOptions[target].type === 'string') {
                this.sortByString(target);
            }
            this.setSortingOptions(target);
            this.sortingOrder = this.SORT_DESC;
        }
    };
    ContributorsComponent.prototype.sortByString = function (key) {
        this.sortingService.sortContributorsByString(this.contributors, key);
    };
    ContributorsComponent.prototype.sortByNumber = function (key) {
        this.sortingService.sortContributorsByNumber(this.contributors, key);
    };
    ContributorsComponent.prototype.setSortingOptions = function (target) {
        for (var key in this.sortingOptions) {
            this.sortingOptions[key].sorted = (key === target) ? true : false;
        }
    };
    ContributorsComponent.prototype.toggleSortingOrder = function () {
        this.sortingOrder = (this.sortingOrder === this.SORT_DESC) ? this.SORT_ASC : this.SORT_DESC;
    };
    ContributorsComponent.prototype.setSortArrowClass = function (target) {
        return (!this.sortingOptions[target].sorted) ? 'not-sorted' :
            (this.sortingOptions[target].sorted && this.sortingOrder === this.SORT_DESC) ? 'sorted-descended' : 'sorted-ascended';
    };
    return ContributorsComponent;
}());
ContributorsComponent = __decorate([
    core_1.Component({
        selector: 'contributors-list',
        templateUrl: './contributors.component.html',
        styleUrls: ['./contributors.component.css'],
    }),
    __metadata("design:paramtypes", [contributor_service_1.ContributorService,
        sorting_service_1.SortingService])
], ContributorsComponent);
exports.ContributorsComponent = ContributorsComponent;
//# sourceMappingURL=contributors.component.js.map
"use strict";
var contributors_component_1 = require("./contributors.component");
var component;
describe('toggleSortingOrder (ContributorsComponent)', function () {
    beforeEach(function () {
        component = new contributors_component_1.ContributorsComponent(null, null);
    });
    it('should set sorting order to ascending', function () {
        component.sortingOrder = component.SORT_DESC;
        component.toggleSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_ASC);
    });
    it('should set sorting order to descended', function () {
        component.sortingOrder = component.SORT_ASC;
        component.toggleSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_DESC);
    });
    it('should return initial value on double toggling sorting order', function () {
        component.sortingOrder = component.SORT_DESC;
        component.toggleSortingOrder();
        component.toggleSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_DESC);
    });
});
describe('setSortingOptions (ContributorsComponent)', function () {
    beforeEach(function () {
        component = new contributors_component_1.ContributorsComponent(null, null);
    });
    it('should set sorting by nickname to true, others to false', function () {
        component.setSortingOptions('nickname');
        expect(component.sortingOptions.nickname.sorted === true &&
            component.sortingOptions.team.sorted === false &&
            component.sortingOptions.contributions.sorted === false).toBeTruthy();
    });
    it('should set sorting by team to true, others to false', function () {
        component.setSortingOptions('team');
        expect(component.sortingOptions.nickname.sorted === false &&
            component.sortingOptions.team.sorted === true &&
            component.sortingOptions.contributions.sorted === false).toBeTruthy();
    });
    it('should set sorting by contributions to true, others to false', function () {
        component.setSortingOptions('contributions');
        expect(component.sortingOptions.nickname.sorted === false &&
            component.sortingOptions.team.sorted === false &&
            component.sortingOptions.contributions.sorted === true).toBeTruthy();
    });
});
describe('setSortArrowClass (ContributorsComponent)', function () {
    beforeEach(function () {
        component = new contributors_component_1.ContributorsComponent(null, null);
    });
    it('should set class "sorted-descended" for nickname field', function () {
        component.sortingOptions.nickname.sorted = true;
        component.sortingOrder = component.SORT_DESC;
        var result = component.setSortArrowClass('nickname');
        expect(result).toBe('sorted-descended');
    });
    it('should set class "sorted-ascended" for nickname field', function () {
        component.sortingOptions.nickname.sorted = true;
        component.sortingOrder = component.SORT_ASC;
        var result = component.setSortArrowClass('nickname');
        expect(result).toBe('sorted-ascended');
    });
    it('should set class "not-sorted" for nickname field', function () {
        component.sortingOptions.nickname.sorted = false;
        var result = component.setSortArrowClass('nickname');
        expect(result).toBe('not-sorted');
    });
    it('should set class "sorted-descended" for team field', function () {
        component.sortingOptions.team.sorted = true;
        component.sortingOrder = component.SORT_DESC;
        var result = component.setSortArrowClass('team');
        expect(result).toBe('sorted-descended');
    });
    it('should set class "sorted-ascended" for team field', function () {
        component.sortingOptions.team.sorted = true;
        component.sortingOrder = component.SORT_ASC;
        var result = component.setSortArrowClass('team');
        expect(result).toBe('sorted-ascended');
    });
    it('should set class "not-sorted" for nickname field', function () {
        component.sortingOptions.team.sorted = false;
        var result = component.setSortArrowClass('team');
        expect(result).toBe('not-sorted');
    });
    it('should set class "sorted-descended" for contributions field', function () {
        component.sortingOptions.contributions.sorted = true;
        component.sortingOrder = component.SORT_DESC;
        var result = component.setSortArrowClass('contributions');
        expect(result).toBe('sorted-descended');
    });
    it('should set class "sorted-ascended" for contributions field', function () {
        component.sortingOptions.contributions.sorted = true;
        component.sortingOrder = component.SORT_ASC;
        var result = component.setSortArrowClass('contributions');
        expect(result).toBe('sorted-ascended');
    });
    it('should set class "not-sorted" for contributions field', function () {
        component.sortingOptions.contributions.sorted = false;
        var result = component.setSortArrowClass('contributions');
        expect(result).toBe('not-sorted');
    });
});
//# sourceMappingURL=contributors.component.spec.js.map
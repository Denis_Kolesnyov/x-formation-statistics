import { ContributorsComponent } from './contributors.component';

let component: ContributorsComponent;

describe('toggleSortingOrder (ContributorsComponent)', () => {

  beforeEach(() => {
    component = new ContributorsComponent(null, null);
  });

  it('should set sorting order to ascending', () => {
    component.sortingOrder = component.SORT_DESC;
    component.toggleSortingOrder();
    expect(component.sortingOrder).toBe(component.SORT_ASC);
  });

  it('should set sorting order to descended', () => {
    component.sortingOrder = component.SORT_ASC;
    component.toggleSortingOrder();
    expect(component.sortingOrder).toBe(component.SORT_DESC);
  });

  it('should return initial value on double toggling sorting order', () => {
    component.sortingOrder = component.SORT_DESC;
    component.toggleSortingOrder();
    component.toggleSortingOrder();
    expect(component.sortingOrder).toBe(component.SORT_DESC);
  });
});

describe('setSortingOptions (ContributorsComponent)', () => {

  beforeEach(() => {
    component = new ContributorsComponent(null, null);
  });

  it('should set sorting by nickname to true, others to false', () => {
    component.setSortingOptions('nickname');
    expect(component.sortingOptions.nickname.sorted === true &&
           component.sortingOptions.team.sorted === false &&
           component.sortingOptions.contributions.sorted === false).toBeTruthy();
  });

  it('should set sorting by team to true, others to false', () => {
    component.setSortingOptions('team');
    expect(component.sortingOptions.nickname.sorted === false &&
      component.sortingOptions.team.sorted === true &&
      component.sortingOptions.contributions.sorted === false).toBeTruthy();
  });

  it('should set sorting by contributions to true, others to false', () => {
    component.setSortingOptions('contributions');
    expect(component.sortingOptions.nickname.sorted === false &&
      component.sortingOptions.team.sorted === false &&
      component.sortingOptions.contributions.sorted === true).toBeTruthy();
  });
});

describe('setSortArrowClass (ContributorsComponent)', () => {
  beforeEach(() => {
    component = new ContributorsComponent(null, null);
  });

  it('should set class "sorted-descended" for nickname field', () => {
    component.sortingOptions.nickname.sorted = true;
    component.sortingOrder = component.SORT_DESC;
    let result = component.setSortArrowClass('nickname');
    expect(result).toBe('sorted-descended');
  });

  it('should set class "sorted-ascended" for nickname field', () => {
    component.sortingOptions.nickname.sorted = true;
    component.sortingOrder = component.SORT_ASC;
    let result = component.setSortArrowClass('nickname');
    expect(result).toBe('sorted-ascended');
  });

  it('should set class "not-sorted" for nickname field', () => {
    component.sortingOptions.nickname.sorted = false;
    let result = component.setSortArrowClass('nickname');
    expect(result).toBe('not-sorted');
  });

  it('should set class "sorted-descended" for team field', () => {
    component.sortingOptions.team.sorted = true;
    component.sortingOrder = component.SORT_DESC;
    let result = component.setSortArrowClass('team');
    expect(result).toBe('sorted-descended');
  });

  it('should set class "sorted-ascended" for team field', () => {
    component.sortingOptions.team.sorted = true;
    component.sortingOrder = component.SORT_ASC;
    let result = component.setSortArrowClass('team');
    expect(result).toBe('sorted-ascended');
  });

  it('should set class "not-sorted" for nickname field', () => {
    component.sortingOptions.team.sorted = false;
    let result = component.setSortArrowClass('team');
    expect(result).toBe('not-sorted');
  });

  it('should set class "sorted-descended" for contributions field', () => {
    component.sortingOptions.contributions.sorted = true;
    component.sortingOrder = component.SORT_DESC;
    let result = component.setSortArrowClass('contributions');
    expect(result).toBe('sorted-descended');
  });

  it('should set class "sorted-ascended" for contributions field', () => {
    component.sortingOptions.contributions.sorted = true;
    component.sortingOrder = component.SORT_ASC;
    let result = component.setSortArrowClass('contributions');
    expect(result).toBe('sorted-ascended');
  });

  it('should set class "not-sorted" for contributions field', () => {
    component.sortingOptions.contributions.sorted = false;
    let result = component.setSortArrowClass('contributions');
    expect(result).toBe('not-sorted');
  });
});



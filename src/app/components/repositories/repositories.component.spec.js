"use strict";
var repositories_component_1 = require("./repositories.component");
var repository_1 = require("../../classes/repository");
var component;
var repository1 = new repository_1.Repository(1, 'ARep', 'ARep', 'https://ARep', 'ARep description', '2017-01-01', 1, 'ARepLang', 1, 1);
var repository2 = new repository_1.Repository(2, 'BRep', 'BRep', 'https://BRep', 'BRep description', '2016-01-01', 2, 'BRepLang', 2, 2);
var repository3 = new repository_1.Repository(3, 'CRep', 'CRep', 'https://CRep', 'CRep description', '2015-01-01', 3, 'CRepLang', 3, 3);
var reversedRepositories;
describe('toggleSortingOrder (RepositoriesComponent)', function () {
    beforeEach(function () {
        component = new repositories_component_1.RepositoriesComponent(null, null);
    });
    it('should set sorting order to ascending', function () {
        component.sortingOrder = component.SORT_DESC;
        component.toggleSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_ASC);
    });
    it('should set sorting order to descended', function () {
        component.sortingOrder = component.SORT_ASC;
        component.toggleSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_DESC);
    });
    it('should return initial value on double toggling sorting order', function () {
        component.sortingOrder = component.SORT_DESC;
        component.toggleSortingOrder();
        component.toggleSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_DESC);
    });
});
describe('switchSortingOrder (RepositoriesComponent)', function () {
    beforeEach(function () {
        component = new repositories_component_1.RepositoriesComponent(null, null);
    });
    it('should toggle sorting order to "ascending" and reverse array', function () {
        component.sortingOrder = component.SORT_DESC;
        component.repositories = [repository3, repository2, repository1];
        component.switchSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_ASC);
        expect(component.repositories[0]).toBe(repository1);
    });
    it('should toggle sorting order to "descending" and reverse array', function () {
        component.sortingOrder = component.SORT_ASC;
        component.repositories = [repository1, repository2, repository3];
        component.switchSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_DESC);
        expect(component.repositories[0]).toBe(repository3);
    });
    it('should toggle sorting order and reverse array twice and return initial values', function () {
        component.sortingOrder = component.SORT_DESC;
        component.repositories = [repository3, repository2, repository1];
        component.switchSortingOrder();
        component.switchSortingOrder();
        expect(component.sortingOrder).toBe(component.SORT_DESC);
        expect(component.repositories[0]).toBe(repository3);
    });
});
describe('applySortingOrder (RepositoriesComponent)', function () {
    beforeEach(function () {
        component = new repositories_component_1.RepositoriesComponent(null, null);
        reversedRepositories = [repository3, repository2, repository1];
    });
    it('should reverse descended array if selected "ascending" option', function () {
        component.sortingOrder = component.SORT_ASC;
        component.repositories = [repository1, repository2, repository3];
        component.applySortingOrder();
        expect(component.repositories[0]).toBe(repository3);
    });
    it('should not do anything if selected "descending" option', function () {
        component.sortingOrder = component.SORT_DESC;
        component.repositories = [repository3, repository2, repository1];
        component.applySortingOrder();
        expect(component.repositories[0]).toBe(repository3);
    });
});
//# sourceMappingURL=repositories.component.spec.js.map
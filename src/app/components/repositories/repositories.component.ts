import { Component, OnInit } from '@angular/core';
import { Repository } from '../../classes/repository';
import { RepositoryService } from '../../services/repository.service';
import { SortingService } from '../../services/sorting.service';

@Component({
  selector: 'repositories-list',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.css'],
})

export class RepositoriesComponent implements OnInit {
  repositories: Repository[];
  sortingOptions = [
    {name: 'forks',       type: 'number' , objProp: 'forks_count'},
    {name: 'name',        type: 'string' , objProp: 'name'       },
    {name: 'language',    type: 'string',  objProp: 'language'   },
    {name: 'last update', type: 'date',    objProp: 'updated_at' },
  ];
  selectedOption = this.sortingOptions[0];
  SORT_DESC = 'Descending';
  SORT_ASC = 'Ascending';
  sortingOrder = this.SORT_DESC;

  loadRepositoriesList(): void {
      this.repositoryService.getRepositories().subscribe(repositories =>
        this.repositories = this.sortingService.sortRepositoriesByNumber(repositories, 'forks_count'));
  }

  ngOnInit(): void {
     this.loadRepositoriesList();
  }

  onOptionChange(option: any): void {
    this.sortRepositories(option);
    this.applySortingOrder();
  };

  sortRepositories(target: any): void {
    if (target.type === 'number') {
      this.sortByNumber(target.objProp);
    } else if (target.type === 'string') {
      this.sortByString(target.objProp);
    } else if (target.type === 'date') {
      this.sortByDate(target.objProp);
    }
  }

  sortByString(key: string): void {
    this.repositories = this.sortingService.sortRepositoriesByString(this.repositories, key);
  }

  sortByNumber(key: string): void {
    this.repositories = this.sortingService.sortRepositoriesByNumber(this.repositories, key);
  }

  sortByDate(key: string): void {
    this.repositories = this.sortingService.sortRepositoriesByDate(this.repositories, key);
  }

  toggleSortingOrder(): void {
    this.sortingOrder = (this.sortingOrder === this.SORT_DESC) ? this.SORT_ASC : this.SORT_DESC;
  }

  switchSortingOrder(): void {
    this.toggleSortingOrder();
    this.repositories.reverse();
    console.log(this.repositories);
  }

  applySortingOrder(): void {
    if (this.sortingOrder === this.SORT_ASC) {
      this.repositories.reverse();
    }
  }

  constructor(
    private repositoryService: RepositoryService,
    private sortingService: SortingService,
  ) {}
}





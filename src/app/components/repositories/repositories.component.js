"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var repository_service_1 = require("../../services/repository.service");
var sorting_service_1 = require("../../services/sorting.service");
var RepositoriesComponent = (function () {
    function RepositoriesComponent(repositoryService, sortingService) {
        this.repositoryService = repositoryService;
        this.sortingService = sortingService;
        this.sortingOptions = [
            { name: 'forks', type: 'number', objProp: 'forks_count' },
            { name: 'name', type: 'string', objProp: 'name' },
            { name: 'language', type: 'string', objProp: 'language' },
            { name: 'last update', type: 'date', objProp: 'updated_at' },
        ];
        this.selectedOption = this.sortingOptions[0];
        this.SORT_DESC = 'Descending';
        this.SORT_ASC = 'Ascending';
        this.sortingOrder = this.SORT_DESC;
    }
    RepositoriesComponent.prototype.loadRepositoriesList = function () {
        var _this = this;
        this.repositoryService.getRepositories().subscribe(function (repositories) {
            return _this.repositories = _this.sortingService.sortRepositoriesByNumber(repositories, 'forks_count');
        });
    };
    RepositoriesComponent.prototype.ngOnInit = function () {
        this.loadRepositoriesList();
    };
    RepositoriesComponent.prototype.onOptionChange = function (option) {
        this.sortRepositories(option);
        this.applySortingOrder();
    };
    ;
    RepositoriesComponent.prototype.sortRepositories = function (target) {
        if (target.type === 'number') {
            this.sortByNumber(target.objProp);
        }
        else if (target.type === 'string') {
            this.sortByString(target.objProp);
        }
        else if (target.type === 'date') {
            this.sortByDate(target.objProp);
        }
    };
    RepositoriesComponent.prototype.sortByString = function (key) {
        this.repositories = this.sortingService.sortRepositoriesByString(this.repositories, key);
    };
    RepositoriesComponent.prototype.sortByNumber = function (key) {
        this.repositories = this.sortingService.sortRepositoriesByNumber(this.repositories, key);
    };
    RepositoriesComponent.prototype.sortByDate = function (key) {
        this.repositories = this.sortingService.sortRepositoriesByDate(this.repositories, key);
    };
    RepositoriesComponent.prototype.toggleSortingOrder = function () {
        this.sortingOrder = (this.sortingOrder === this.SORT_DESC) ? this.SORT_ASC : this.SORT_DESC;
    };
    RepositoriesComponent.prototype.switchSortingOrder = function () {
        this.toggleSortingOrder();
        this.repositories.reverse();
        console.log(this.repositories);
    };
    RepositoriesComponent.prototype.applySortingOrder = function () {
        if (this.sortingOrder === this.SORT_ASC) {
            this.repositories.reverse();
        }
    };
    return RepositoriesComponent;
}());
RepositoriesComponent = __decorate([
    core_1.Component({
        selector: 'repositories-list',
        templateUrl: './repositories.component.html',
        styleUrls: ['./repositories.component.css'],
    }),
    __metadata("design:paramtypes", [repository_service_1.RepositoryService,
        sorting_service_1.SortingService])
], RepositoriesComponent);
exports.RepositoriesComponent = RepositoriesComponent;
//# sourceMappingURL=repositories.component.js.map
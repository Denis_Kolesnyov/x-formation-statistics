export class Contributor {
  constructor(
    public nickname: string,
    public team: string,
    public contributions: number,
  ) {}
}

"use strict";
var Contributor = (function () {
    function Contributor(nickname, team, contributions) {
        this.nickname = nickname;
        this.team = team;
        this.contributions = contributions;
    }
    return Contributor;
}());
exports.Contributor = Contributor;
//# sourceMappingURL=contributor.js.map
"use strict";
var Repository = (function () {
    function Repository(id, name, full_name, html_url, description, updated_at, stargazers_count, language, forks_count, forks) {
        this.id = id;
        this.name = name;
        this.full_name = full_name;
        this.html_url = html_url;
        this.description = description;
        this.updated_at = updated_at;
        this.stargazers_count = stargazers_count;
        this.language = language;
        this.forks_count = forks_count;
        this.forks = forks;
    }
    return Repository;
}());
exports.Repository = Repository;
//# sourceMappingURL=repository.js.map
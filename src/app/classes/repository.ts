export class Repository {
  constructor(
    public id: number,
    public name: string,
    public full_name: string,
    public html_url: string,
    public description: string,
    public updated_at: string,
    public stargazers_count: number,
    public language: string,
    public forks_count: number,
    public forks: number,
  ) {}
}

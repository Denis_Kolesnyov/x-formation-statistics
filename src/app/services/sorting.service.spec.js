"use strict";
var repository_1 = require("./../classes/repository");
var contributor_1 = require("./../classes/contributor");
var sorting_service_1 = require("./sorting.service");
var service;
var repository1 = new repository_1.Repository(2, 'BRep', 'BRep', 'https://BRep', 'BRep description', '2016-01-01', 2, 'BRepLang', 2, 2);
var repository2 = new repository_1.Repository(3, 'ARep', 'ARep', 'https://ARep', 'ARep description', '2017-01-01', 3, 'ARepLang', 3, 3);
var repository3 = new repository_1.Repository(1, 'CRep', 'CRep', 'https://CRep', 'CRep description', '2015-01-01', 1, 'CRepLang', 1, 1);
var repositories;
var sortedRepositories;
var contributor1 = new contributor_1.Contributor('John', 'JTeam', 5);
var contributor2 = new contributor_1.Contributor('Albert', 'ATeam', 10);
var contributor3 = new contributor_1.Contributor('Zak', 'ZTeam', 1);
var contributors;
var sortedContributors;
describe('sortContributorsByString (SortingService)', function () {
    beforeEach(function () {
        service = new sorting_service_1.SortingService();
        contributors = [contributor1, contributor2, contributor3];
        sortedContributors = [contributor2, contributor1, contributor3];
    });
    it('should sort contributors by nickname', function () {
        expect(service.sortContributorsByString(contributors, 'nickname')).toEqual(sortedContributors);
    });
    it('should sort contributors by team', function () {
        expect(service.sortContributorsByString(contributors, 'team')).toEqual(sortedContributors);
    });
});
describe('sortContributorsByNumber (SortingService)', function () {
    beforeEach(function () {
        service = new sorting_service_1.SortingService();
        contributors = [contributor1, contributor2, contributor3];
        sortedContributors = [contributor2, contributor1, contributor3];
    });
    it('should sort contributors by contributions amount', function () {
        expect(service.sortContributorsByNumber(contributors, 'contributions')).toEqual(sortedContributors);
    });
});
describe('sortRepositoriesByString (SortingService)', function () {
    beforeEach(function () {
        service = new sorting_service_1.SortingService();
        repositories = [repository1, repository2, repository3];
        sortedRepositories = [repository2, repository1, repository3];
    });
    it('should sort repositories by name', function () {
        expect(service.sortRepositoriesByString(repositories, 'name')).toEqual(sortedRepositories);
    });
    it('should sort repositories by language', function () {
        expect(service.sortRepositoriesByString(repositories, 'language')).toEqual(sortedRepositories);
    });
});
describe('sortRepositoriesByNumber (SortingService)', function () {
    beforeEach(function () {
        service = new sorting_service_1.SortingService();
        repositories = [repository1, repository2, repository3];
        sortedRepositories = [repository2, repository1, repository3];
    });
    it('should sort repositories by forks count', function () {
        expect(service.sortRepositoriesByNumber(repositories, 'forks_count')).toEqual(sortedRepositories);
    });
});
describe('sortRepositoriesByDate (SortingService)', function () {
    beforeEach(function () {
        service = new sorting_service_1.SortingService();
        repositories = [repository1, repository2, repository3];
        sortedRepositories = [repository2, repository1, repository3];
    });
    it('should sort repositories by date', function () {
        expect(service.sortRepositoriesByDate(repositories, 'updated_at')).toEqual(sortedRepositories);
    });
});
//# sourceMappingURL=sorting.service.spec.js.map
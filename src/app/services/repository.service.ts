import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable ()

export class RepositoryService {
  repositoriesUrl = 'https://api.github.com/users/x-formation/repos';

  getRepositories() {
    return this.http.get(this.repositoriesUrl).map(response => response.json());
  }

  constructor (
    private http: Http,
  ) {}
}

import { Injectable } from '@angular/core';
import { Contributor } from '../classes/contributor';
import { Repository } from '../classes/repository';

@Injectable()

export class SortingService {

  sortContributorsByNumber(contributors: Contributor[], key: string): Contributor[] {
    return contributors.sort(
      (contributor1: Contributor, contributor2: Contributor) =>
      contributor2[key] - contributor1[key]);
  }

  sortContributorsByString(contributors: Contributor[], key: string): Contributor[] {
    return contributors.sort((contributor1: Contributor, contributor2: Contributor) =>
      (contributor1[key] > contributor2[key]) ? 1 : 0);
  }

  sortRepositoriesByNumber(repositories: Repository[], key: string): Repository[] {
    return repositories.sort(
      (repository1: Repository, repository2: Repository) =>
      repository2[key] - repository1[key]);
  }

  sortRepositoriesByString(repositories: Repository[], key: string): Repository[] {
    return repositories.sort((repository1: Repository, repository2: Repository) =>
      (repository1[key] > repository2[key]) ? 1 : 0);
  }

  sortRepositoriesByDate(repositories: Repository[], key: string): Repository[] {
    return repositories.sort((repository1: Repository, repository2: Repository) =>
    Date.parse(repository2[key]) - Date.parse(repository1[key]));
  }
}

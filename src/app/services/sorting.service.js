"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var SortingService = (function () {
    function SortingService() {
    }
    SortingService.prototype.sortContributorsByNumber = function (contributors, key) {
        return contributors.sort(function (contributor1, contributor2) {
            return contributor2[key] - contributor1[key];
        });
    };
    SortingService.prototype.sortContributorsByString = function (contributors, key) {
        return contributors.sort(function (contributor1, contributor2) {
            return (contributor1[key] > contributor2[key]) ? 1 : 0;
        });
    };
    SortingService.prototype.sortRepositoriesByNumber = function (repositories, key) {
        return repositories.sort(function (repository1, repository2) {
            return repository2[key] - repository1[key];
        });
    };
    SortingService.prototype.sortRepositoriesByString = function (repositories, key) {
        return repositories.sort(function (repository1, repository2) {
            return (repository1[key] > repository2[key]) ? 1 : 0;
        });
    };
    SortingService.prototype.sortRepositoriesByDate = function (repositories, key) {
        return repositories.sort(function (repository1, repository2) {
            return Date.parse(repository2[key]) - Date.parse(repository1[key]);
        });
    };
    return SortingService;
}());
SortingService = __decorate([
    core_1.Injectable()
], SortingService);
exports.SortingService = SortingService;
//# sourceMappingURL=sorting.service.js.map
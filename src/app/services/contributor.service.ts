import { Injectable } from '@angular/core';
import { Contributor } from '../classes/contributor';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class ContributorService {
  contributorsUrl = 'api/contributors';

  getContributors(): Promise<Contributor[]> {
    return this.http.get(this.contributorsUrl).toPromise()
      .then(response => response.json().data as Contributor[]);
  }

  constructor(
    private http: Http,
  ) {}
}

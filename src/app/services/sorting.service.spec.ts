import { Repository } from './../classes/repository';
import { Contributor } from './../classes/contributor';
import { SortingService } from './sorting.service';

let service: SortingService;

let repository1 = new Repository(2, 'BRep', 'BRep', 'https://BRep', 'BRep description', '2016-01-01', 2, 'BRepLang', 2, 2);
let repository2 = new Repository(3, 'ARep', 'ARep', 'https://ARep', 'ARep description', '2017-01-01', 3, 'ARepLang', 3, 3);
let repository3 = new Repository(1, 'CRep', 'CRep', 'https://CRep', 'CRep description', '2015-01-01', 1, 'CRepLang', 1, 1);
let repositories: Repository[];
let sortedRepositories: Repository[];

let contributor1 = new Contributor('John', 'JTeam', 5);
let contributor2 = new Contributor('Albert', 'ATeam', 10);
let contributor3 = new Contributor('Zak', 'ZTeam', 1);
let contributors: Contributor[];
let sortedContributors: Contributor[];

describe('sortContributorsByString (SortingService)', () => {
  beforeEach(() => {
    service = new SortingService();
    contributors = [contributor1, contributor2, contributor3];
    sortedContributors = [contributor2, contributor1, contributor3];
  });

  it('should sort contributors by nickname', () => {
    expect(service.sortContributorsByString(contributors, 'nickname')).toEqual(sortedContributors);
  });

  it('should sort contributors by team', () => {
    expect(service.sortContributorsByString(contributors, 'team')).toEqual(sortedContributors);
  });
});

describe('sortContributorsByNumber (SortingService)', () => {
  beforeEach(() => {
    service = new SortingService();
    contributors = [contributor1, contributor2, contributor3];
    sortedContributors = [contributor2, contributor1, contributor3];
  });

  it('should sort contributors by contributions amount', () => {
    expect(service.sortContributorsByNumber(contributors, 'contributions')).toEqual(sortedContributors);
  });
});

describe('sortRepositoriesByString (SortingService)', () => {
  beforeEach(() => {
    service = new SortingService();
    repositories = [repository1, repository2, repository3];
    sortedRepositories = [repository2, repository1, repository3];
  });

  it('should sort repositories by name', () => {
    expect(service.sortRepositoriesByString(repositories, 'name')).toEqual(sortedRepositories);
  });

  it('should sort repositories by language', () => {
    expect(service.sortRepositoriesByString(repositories, 'language')).toEqual(sortedRepositories);
  });
});

describe('sortRepositoriesByNumber (SortingService)', () => {
  beforeEach(() => {
    service = new SortingService();
    repositories = [repository1, repository2, repository3];
    sortedRepositories = [repository2, repository1, repository3];
  });

  it('should sort repositories by forks count', () => {
    expect(service.sortRepositoriesByNumber(repositories, 'forks_count')).toEqual(sortedRepositories);
  });
});

describe('sortRepositoriesByDate (SortingService)', () => {
  beforeEach(() => {
    service = new SortingService();
    repositories = [repository1, repository2, repository3];
    sortedRepositories = [repository2, repository1, repository3];
  });

  it('should sort repositories by date', () => {
    expect(service.sortRepositoriesByDate(repositories, 'updated_at')).toEqual(sortedRepositories);
  });
});

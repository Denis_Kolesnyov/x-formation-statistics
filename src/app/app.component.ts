import { Component } from '@angular/core';
import {SortingService} from './services/sorting.service';

@Component({
  selector: 'my-page',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ SortingService ],
})

export class AppComponent  {
  selectedTab = 'Contributors';

  onTabSelect(value: string): void {
    this.selectedTab = value;
  }
}

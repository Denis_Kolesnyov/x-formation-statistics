# How to start an application #

1. You need to check if the following applications installed on your system:
	1.1 Node - use in terminal command "node -v". If you see an error message instead of node version - use the next link to download node: https://nodejs.org/en/download/
	1.2 (skip this step if you have already installed node on previous step) Npm - use in terminal command "npm -v". If you see the error message instead of npm version - use the link from the step 1.1 (npm included into node package)
	1.3 Git - use in terminal command "git --version". If it throws a error, then you need to install git following this link: https://www.atlassian.com/git/tutorials/install-git#windows
2. Using terminal navigate to the file system directory, where you would like to download the application (e.g. cd/d d:workspace)
3. Paste the following line to the terminal: git clone https://Denis_Kolesnyov@bitbucket.org/Denis_Kolesnyov/x-formation-statistics.git											 
4. The repository will be cloned to the directory. Paste command "cd x-formation-statistics" into terminal.
5. When you are in "x-formation-statistics" folder use command "npm install". Wait a little untill instalation complete.
6. When instalation will be completed you can start an application using command "npm start". It will start a local server and the application will be oppened in browser window.
7. to run unit tests you need to use "npm test" command in terminal.

